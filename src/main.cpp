//
// Created by honza on 25.6.16.
//

#include <stdint.h>
#include <USBAPI.h>
#include <RCSwitch.h>

uint8_t motor_enablers[] = {9, 3};

uint8_t motor1_logic_pins[] = {4, 5};

uint8_t motor2_logic_pins[] = {6, 7};

int16_t incoming_data = 0;
int16_t incoming_data_old = 0;

uint8_t led = 8;

RCSwitch mySwitch = RCSwitch();

void motor_stop();

void drive_forward();

void turn_right();

void turn_left();

void drive_backward();

void setup() {
    for (int i = 0; i < 2; i++) {
        pinMode(motor_enablers[i], OUTPUT);

        pinMode(motor1_logic_pins[i], OUTPUT);

        pinMode(motor2_logic_pins[i], OUTPUT);
    }

    pinMode(led, OUTPUT);

    Serial.begin(9600);
    mySwitch.enableReceive(0);

    digitalWrite(motor_enablers[0], HIGH);
    digitalWrite(motor_enablers[1], HIGH);
}

void loop() {
    if (mySwitch.available()) {
        incoming_data = mySwitch.getReceivedValue();
        if (incoming_data != incoming_data_old) {
            digitalWrite(led, LOW);
            switch (incoming_data) {
                case 48:
                    motor_stop();
                    Serial.write("stop");
                    break;
                case 56:
                    Serial.write("forward");
                    drive_forward();
                    break;
                case 54:
                    turn_right();
                    Serial.write("right");
                    break;
                case 52:
                    turn_left();
                    Serial.write("left");
                    break;
                case 50:
                    drive_backward();
                    Serial.write("backwards");
                    break;
                default:
                    Serial.write(incoming_data);
                    digitalWrite(led, HIGH);
            }

            incoming_data_old = incoming_data;
        }

        mySwitch.resetAvailable();
    }
}

void motor_stop() {
    digitalWrite(motor1_logic_pins[0], LOW);
    digitalWrite(motor1_logic_pins[1], LOW);

    digitalWrite(motor2_logic_pins[0], LOW);
    digitalWrite(motor2_logic_pins[1], LOW);
    delay(25);
}

void drive_forward() {
    digitalWrite(motor1_logic_pins[0], HIGH);
    digitalWrite(motor1_logic_pins[1], LOW);

    digitalWrite(motor2_logic_pins[0], HIGH);
    digitalWrite(motor2_logic_pins[1], LOW);
}

void drive_backward() {
    digitalWrite(motor1_logic_pins[0], LOW);
    digitalWrite(motor1_logic_pins[1], HIGH);

    digitalWrite(motor2_logic_pins[0], LOW);
    digitalWrite(motor2_logic_pins[1], HIGH);
}

void turn_left() {
    digitalWrite(motor1_logic_pins[0], LOW);
    digitalWrite(motor1_logic_pins[1], HIGH);

    digitalWrite(motor2_logic_pins[0], HIGH);
    digitalWrite(motor2_logic_pins[1], LOW);
}

void turn_right() {
    digitalWrite(motor1_logic_pins[0], HIGH);
    digitalWrite(motor1_logic_pins[1], LOW);

    digitalWrite(motor2_logic_pins[0], LOW);
    digitalWrite(motor2_logic_pins[1], HIGH);
}